require('dotenv').config();
const express = require('express'),
      bodyParser = require('body-parser'),
      path = require('path'),
      cors = require('cors');

const app= express();
const APP_PORT = process.env.PORT;
app.use(bodyParser.json({limit: '50mb'}));
app.use(cors());

app.get('/dice', (req, res, next)=>{
    let roll = Math.floor(Math.random() * 6) + 1;
    console.log(roll);
    res.status(200);
    res.format({
        'text/html': ()=>{
            console.log("returning html");
            res.type('text/plain');
            res.end(`<h1>Dice roll is ${roll}</h1>`);
        },
        'application/json': ()=>{
            console.log("returning json");
            res.json({dice_roll: roll});
        } 
    });
});

app.listen(APP_PORT, ()=>{
    console.log(`App Server listening on ${APP_PORT}`)
})
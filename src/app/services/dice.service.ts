import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DiceService {

  constructor(private httpSvc: HttpClient) { }

  roll(): Promise<any>{
    let headers = new HttpHeaders()
      .set('Accept', 'application/json');

    return this.httpSvc
      .get(environment.api_url,{headers: headers})
      .toPromise();
  }

  getRollDescription(): Promise<any>{
    let headers = new HttpHeaders()
        .set('Accept', 'text/html')
        .set('Content-Type', 'text/html');
        
    return this.httpSvc
      .get(environment.api_url,{headers: headers, responseType: 'text'})
      .toPromise();
  }
}

import { Component, OnInit } from '@angular/core';
import { DiceService } from './services/dice.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'day13-worksheet';
  randomDiceImage?: string;
  randomDiceDesc: string;

  constructor(private diceSvc: DiceService){}

  ngOnInit(){
    this.letsRoll();
  }

  letsRoll(){
    this.diceSvc.roll().then((result)=>{
      this.randomDiceImage = result.dice_roll;
    });

    this.diceSvc.getRollDescription().then((resultDesc)=>{
      console.log(resultDesc);
      this.randomDiceDesc = resultDesc;
    });
  }

  roll(){
    this.letsRoll();
  }
}
